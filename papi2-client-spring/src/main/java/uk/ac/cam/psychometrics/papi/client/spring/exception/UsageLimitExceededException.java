package uk.ac.cam.psychometrics.papi.client.spring.exception;

public class UsageLimitExceededException extends PapiClientException {

    public UsageLimitExceededException(String message, Throwable cause) {
        super(message, cause);
    }
}
