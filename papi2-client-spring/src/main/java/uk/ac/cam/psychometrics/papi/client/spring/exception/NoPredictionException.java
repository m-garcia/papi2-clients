package uk.ac.cam.psychometrics.papi.client.spring.exception;

public class NoPredictionException extends Exception {

    public NoPredictionException(String message) {
        super(message);
    }

    public NoPredictionException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
