package uk.ac.cam.psychometrics.papi.client.spring;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import uk.ac.cam.psychometrics.papi.client.spring.exception.*;
import uk.ac.cam.psychometrics.papi.client.spring.model.Prediction;
import uk.ac.cam.psychometrics.papi.client.spring.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.spring.model.Trait;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PredictionResourceImplTest {

    private static final String RESOURCE_URL = "http://localhost";
    private static final Collection<Trait> TRAITS = new ArrayList<Trait>() {{
        add(Trait.BIG5);
        add(Trait.GAY);
    }};
    private static final String REQUEST_URL = "http://localhost/like_ids?&traits=BIG5,Gay&uid=1";
    private static final String TOKEN = "token";
    private static final String UID = "1";
    private static final Set<String> LIKE_IDS = new HashSet<String>() {{
        add("1");
        add("2");
        add("3");
    }};

    @Mock
    private RestTemplate restTemplate;

    private PredictionResourceImpl predictionResource;

    @Before
    public void setUp() {
        predictionResource = new PredictionResourceImpl(restTemplate, RESOURCE_URL);
    }

    @Test
    public void shouldReturnPersonality() throws Exception {
        PredictionResult predictionList = createPredictionResult();
        when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(createResponseEntity(predictionList));
        PredictionResult returnedPredictionList = predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
        assertEquals(predictionList, returnedPredictionList);
        verifyRequest();
    }

    private PredictionResult createPredictionResult() {
        return new PredictionResult(UID, new HashSet<Prediction>() {{
            add(new Prediction(Trait.BIG5_OPENNESS, 1));
            add(new Prediction(Trait.BIG5_CONSCIENTIOUSNESS, 1));
            add(new Prediction(Trait.BIG5_EXTRAVERSION, 1));
            add(new Prediction(Trait.BIG5_AGREEABLENESS, 1));
            add(new Prediction(Trait.BIG5_NEUROTICISM, 1));
        }});
    }

    private ResponseEntity<PredictionResult> createResponseEntity(PredictionResult predictionList) {
        return new ResponseEntity<>(predictionList, HttpStatus.OK);
    }

    private void verifyRequest() {
        ArgumentCaptor<String> requestUrlCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        verify(restTemplate, times(1)).postForEntity(requestUrlCaptor.capture(), httpEntityCaptor.capture(), eq(PredictionResult.class));
        assertEquals(REQUEST_URL, requestUrlCaptor.getValue());
        assertEquals(LIKE_IDS, httpEntityCaptor.getValue().getBody());
        assertEquals(MediaType.APPLICATION_JSON, httpEntityCaptor.getValue().getHeaders().getContentType());

    }

    @Test
    public void shouldThrowExceptionOnUnauthorized() throws Exception {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (TokenInvalidException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionOnForbidden() throws Exception {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN));
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (TokenExpiredException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionOnMethodNotAllowed() throws Exception {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(new HttpClientErrorException(HttpStatus.METHOD_NOT_ALLOWED));
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (UsageLimitExceededException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionOnNotFound() {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(new ResponseEntity<>(null, HttpStatus.NO_CONTENT));
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (NoPredictionException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionOnUnknownStatus() throws Exception {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (PapiClientException expected) {
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldThrowExceptionOnUnknownException() throws Exception {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(Exception.class);
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (PapiClientException expected) {
        }
    }

}