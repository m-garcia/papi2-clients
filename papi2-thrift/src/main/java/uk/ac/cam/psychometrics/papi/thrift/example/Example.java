package uk.ac.cam.psychometrics.papi.thrift.example;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import uk.ac.cam.psychometrics.papi.thrift.TPapiService;
import uk.ac.cam.psychometrics.papi.thrift.model.TPapiToken;
import uk.ac.cam.psychometrics.papi.thrift.model.TPredictionResult;
import uk.ac.cam.psychometrics.papi.thrift.model.TUserLikeIds;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * To compile this example with Maven, add the following artifacts as dependencies:
 * org.apache.thrift/libthrift-0.9.1
 */
public class Example {

    private static final Logger LOGGER = Logger.getLogger("Example");
    private static final String SERVICE_URL = "api.applymagicsauce.com";
    private static final int SERVICE_PORT = 9901;
    private static final int TIMEOUT = 10000;
    private static final int CUSTOMER_ID = 1;
    private static final String API_KEY = "apiKey";
    private static final String UID = "1";
    private static final Set<String> LIKE_IDS = new HashSet<String>() {{
        add("7010901522");
        add("7721750727");
        add("7557552517");
        add("8536905548");
        add("7723400562");
        add("8800570812");
        add("10765693196");
        add("14269799090");
        add("12938634034");
        add("14287253499");
    }};

    public static void main(String[] args) throws TException {
        new Example().run();
    }

    private void run() throws TException {
        // Creates clients
        TTransport transport = new TSocket(SERVICE_URL, SERVICE_PORT, TIMEOUT);
        transport.open();
        TPapiService.Client papiClient = new TPapiService.Client(new TCompactProtocol(transport));

        // Authentication. This token will be valid for at least one hour, so you want to store it
        // and re-use for further requests
        TPapiToken papiToken = papiClient.requestToken(CUSTOMER_ID, API_KEY);

        // Gets predictions
        List<TPredictionResult> predictionResultList = papiClient.getBatchByLikeIds(
                new ArrayList<String>() {{
                    add("BIG5");
                    add("Religion");
                }},
                papiToken.getTokenString(), createTUserLikeIds(10));

        for (TPredictionResult predictionResult : predictionResultList) {
            LOGGER.info(predictionResult.toString());
        }
    }

    private List<TUserLikeIds> createTUserLikeIds(int howMany) {
        List<TUserLikeIds> tUserLikeIdsList = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            TUserLikeIds tUserLikeIds = new TUserLikeIds();
            tUserLikeIds.setUid(UID);
            tUserLikeIds.setLikeIds(LIKE_IDS);
            tUserLikeIdsList.add(tUserLikeIds);
        }
        return tUserLikeIdsList;
    }
}
