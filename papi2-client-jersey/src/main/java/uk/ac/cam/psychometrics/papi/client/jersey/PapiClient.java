package uk.ac.cam.psychometrics.papi.client.jersey;

import javax.ws.rs.client.Client;

public class PapiClient {

    private static final String ERROR_RESOURCE_CONFIG = "Http endpoint must be configured to use this resource";

    public static final class Config {
        private Client httpClient;
        private String serviceUrl;

        public Config() {
        }

        public Config withHttpEndpoint(Client httpClient, String serviceUrl) {
            this.httpClient = httpClient;
            this.serviceUrl = serviceUrl;
            return this;
        }

    }

    private final Resource authResource;
    private final Resource predictionResource;

    public PapiClient(Config config) {
        authResource = new AuthResourceImpl(config.httpClient, config.serviceUrl + "/auth");
        predictionResource = new PredictionResourceImpl(config.httpClient, config.serviceUrl);
    }

    public AuthResource getAuthResource() {
        if (authResource.isConfigured()) {
            return (AuthResource) authResource;
        } else {
            throw new IllegalStateException(ERROR_RESOURCE_CONFIG);
        }
    }

    public PredictionResource getPredictionResource() {
        if (predictionResource.isConfigured()) {
            return (PredictionResource) predictionResource;
        } else {
            throw new IllegalStateException(ERROR_RESOURCE_CONFIG);
        }
    }

}
