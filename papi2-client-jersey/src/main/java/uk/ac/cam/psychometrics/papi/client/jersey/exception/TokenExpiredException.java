package uk.ac.cam.psychometrics.papi.client.jersey.exception;

public class TokenExpiredException extends PapiClientException {

    public TokenExpiredException(final String message) {
        super(message);
    }

}
