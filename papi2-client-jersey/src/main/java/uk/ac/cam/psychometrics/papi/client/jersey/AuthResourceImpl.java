package uk.ac.cam.psychometrics.papi.client.jersey;

import uk.ac.cam.psychometrics.papi.client.jersey.exception.NotAuthenticatedException;
import uk.ac.cam.psychometrics.papi.client.jersey.exception.PapiClientException;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PapiToken;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

class AuthResourceImpl implements AuthResource, Resource {

    private final Client client;
    private final String resourceUrl;

    AuthResourceImpl(Client client, String resourceUrl) {
        this.client = client;
        this.resourceUrl = resourceUrl;
    }

    @Override
    public PapiToken requestToken(int customerId, String apiKey) {
        Response response = getRequestTokenResponse(resourceUrl, createRequestTokenBody(customerId, apiKey));
        if (response.getStatus() == 200) {
            return response.readEntity(PapiToken.class);
        }
        handleError(customerId, response);
        throw new AssertionError("Execution should never reach here");

    }

    private void handleError(int customerId, Response response) {
        String errorMessage = response.readEntity(String.class);
        String message = String.format("Service has returned %d status for customerId=%d, errorMessage=%s",
                response.getStatus(), customerId, errorMessage);
        if (response.getStatus() == 401) {
            throw new NotAuthenticatedException(message);
        } else {
            throw new PapiClientException(message);
        }
    }

    private String createRequestTokenBody(int customerId, String apiKey) {
        String body = "{ \"customer_id\": %d, \"api_key\": \"%s\" }";
        return String.format(body, customerId, apiKey);
    }

    // @VisibleForTesting
    Response getRequestTokenResponse(String url, String body) {
        try {
            return client.target(url).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(body, MediaType.APPLICATION_JSON_TYPE));
        } catch (Exception e) {
            throw new PapiClientException(String.format("Unexpected exception occurred on request to url=%s", url), e);
        }
    }

    @Override
    public boolean isConfigured() {
        return client != null && resourceUrl != null && !resourceUrl.isEmpty();
    }
}