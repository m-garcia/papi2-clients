package uk.ac.cam.psychometrics.papi.client.jersey.exception;

@SuppressWarnings("WeakerAccess")
public class PapiClientException extends RuntimeException {

    public PapiClientException(final String message) {
        super(message);
    }

    public PapiClientException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
