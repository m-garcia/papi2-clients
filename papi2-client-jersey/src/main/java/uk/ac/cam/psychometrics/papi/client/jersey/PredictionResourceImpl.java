package uk.ac.cam.psychometrics.papi.client.jersey;

import uk.ac.cam.psychometrics.papi.client.jersey.exception.*;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.jersey.model.Trait;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class PredictionResourceImpl implements PredictionResource, Resource {

    private final Client client;
    private final String resourceUrl;

    public PredictionResourceImpl(Client client, String resourceUrl) {
        this.client = client;
        this.resourceUrl = resourceUrl;
    }

    @Override
    public PredictionResult getByLikeIds(Collection<Trait> traits, String token, String uid, Set<String> likeIds) throws NoPredictionException {
        Response response = getByLikeIdsResponse(resourceUrl + "/like_ids", traits, token, uid, likeIds);
        if (response.getStatus() == 200) {
            return response.readEntity(PredictionResult.class);
        }
        handleError(traits, token, uid, likeIds, response);
        throw new AssertionError("Execution should never reach here");
    }

    // @VisibleForTesting
    Response getByLikeIdsResponse(String url, Collection<Trait> traits, String token, String uid, Set<String> likeIds) {
        try {
            WebTarget webTarget = client.target(url);
            if (traits != null && !traits.isEmpty()) {
                webTarget.queryParam("traits", toCsvString(traits));
            }
            if (uid != null) {
                webTarget = webTarget.queryParam("uid", String.valueOf(uid));
            }
            return webTarget
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("X-Auth-Token", token)
                    .post(Entity.entity(likeIds, MediaType.APPLICATION_JSON_TYPE));
        } catch (Exception e) {
            throw new PapiClientException(String.format("Unexpected exception occurred on request to url=%s", url), e);
        }
    }

    private void handleError(Collection<Trait> traits, String token, String uid, Set<String> likeIds, Response response) throws NoPredictionException {
        String errorMessage = response.readEntity(String.class);
        String message = String.format("Service has returned %d status for traits=%s, token=%s, uid=%s, " +
                "likeIds=%s, errorMessage=%s",
                response.getStatus(), toCsvString(traits), token, uid, likeIds, errorMessage);
        if (response.getStatus() == 401) {
            throw new TokenInvalidException(message);
        }
        if (response.getStatus() == 403) {
            throw new TokenExpiredException(message);
        }
        if (response.getStatus() == 405) {
            throw new UsageLimitExceededException(message);
        }
        if (response.getStatus() == 204) {
            throw new NoPredictionException(message);
        } else {
            throw new PapiClientException(message);
        }
    }

    private String toCsvString(Collection<Trait> traits) {
        if (traits != null && !traits.isEmpty()) {
            StringBuilder joinedString = new StringBuilder();
            Iterator<Trait> iterator = traits.iterator();
            while (iterator.hasNext()) {
                joinedString.append(iterator.next().getName());
                if (iterator.hasNext()) {
                    joinedString.append(",");
                }
            }
            return joinedString.toString();
        } else {
            return null;
        }
    }

    @Override
    public boolean isConfigured() {
        return client != null && resourceUrl != null && !resourceUrl.isEmpty();
    }
}
